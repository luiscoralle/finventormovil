# Project Information
<p><b>Name Project :</b>FInventorMovil</p>
<p><b>Last version  :</b>1.0.0</p>
<p><b>Last updated :</b> 01/10/2018</p>
<p><b>Programming language :</b> Bash</p>
<p><b>Youtube : </b>Pronto</p>
<!--<p><b>Company name : </b><a target="_blank" href=""></a> (stic)</p></h6> -->
<p><b>University: </b><a target="_blank" href="http://faiweb.uncoma.edu.ar/"></a>Facultad de Informatica - Universidad Nacional del Comahue</p>

<h4>FInventorMovil</h4>
<p>
FInventorMovil es una recolección de software que permite ejecutar en una Máquina Virtual un servidor de App Inventor para que varios usuarios puedan conectarse via wifi y desarrolar aplicaciones para Android sin conección a Internet. Cada usuario puede, de forma visual y a partir de un conjunto de herramientas básicas, ir enlazando una serie de bloques para crear la aplicación. Las aplicaciones creadas con App Inventor están limitadas por su simplicidad, aunque permiten cubrir un gran número de necesidades básicas en un dispositivo móvil.
</p>


# Contacts
<ul>
<li>   Author      :   Luis G. Coralle
<li>   Linkedin    :   https://www.linkedin.com/in/luiscoralle/
<li>   E-Mail      :   luiscoralle@fi.uncoma.edu.ar
<li>   GitLab      :   https://gitlab.com/luiscoralle/finventormovil
</ul>
